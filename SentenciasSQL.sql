use musica;

insert into disqueras (nombre) values ('Warnner Music');
select * from Disqueras;

desc Artistas;
insert into artistas (nombre,Disqueras_idDisquera) values ('MonLaferte',5);
select * from artistas;

insert into Albums (nombre,fecha,Artistas_idArtista,Artistas_Disqueras_idDisquera) values ('La Trenza','2017-5-5',4,5);
select * from albums;

insert into Canciones (nombre,duracion, Albums_idAlbum,Albums_Artistas_idArtista,Albums_Artistas_Disqueras_idDisquera) values
                      ('Cuando Era Flor',4.41,7,4,5);
                      
delete from Canciones where idCancion;
delete from Albums where idAlbum;
delete from artistas where idartista;
delete from disqueras where iddisquera;

select c.nombre, 
       c.duracion, 
       al.nombre, 
       al.fecha, 
       ar.nombre, 
       d.nombre 
  from Canciones c 
    inner join albums al on c.Albums_idAlbum = al.idAlbum
    inner join artistas ar on c.Albums_Artistas_idArtista = ar.idArtista
    inner join disqueras d on c.Albums_Artistas_Disqueras_idDisquera = d.idDisquera; 
    
select ar.idArtista, 
       d.idDisquera 
  from Albums c 
    inner join artistas ar on c.Artistas_idArtista = ar.idArtista
    inner join disqueras d on c.Artistas_Disqueras_idDisquera = d.idDisquera; 