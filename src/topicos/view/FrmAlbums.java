package topicos.view;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import topicos.model.ConexionBD;

public class FrmAlbums extends javax.swing.JFrame {

    DefaultTableModel modeloTabla;
    DefaultComboBoxModel modeloCombo1;
    DefaultComboBoxModel modeloCombo2;
    ConexionBD bd;
    int selectedRow = -1;
    int idArt = 0;
    int idDis = 0;
    String selectedId;

    public void SeleccionarDisquera(String nombre) {
        try {
            this.bd.res = bd.stat.executeQuery("select idDisquera from Disqueras where nombre = '" + nombre + "';");
            while (bd.res.next()) {
                idDis = bd.res.getInt("idDisquera");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void SeleccionarArtista(String nombre) {
        try {
            this.bd.res = bd.stat.executeQuery("select idArtista from Artistas where nombre = '" + nombre + "';");
            while (bd.res.next()) {
                idArt = bd.res.getInt("idArtista");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void agregarAlbums(String nombre, String fecha, String idArtista, String idDisquera) {
        idArtista = Com_Art.getSelectedItem().toString().trim();
        idDisquera = Com_Dis.getSelectedItem().toString().trim();
        SeleccionarArtista(idArtista);
        SeleccionarDisquera(idDisquera);

        String stmtSQL = "insert into albums (nombre, fecha, Artistas_idArtista, Artistas_Disqueras_idDisquera) "
                + "values ('" + nombre + "','" + fecha + "','" + idArt + "','" + idDis + "');";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en insercion de registro");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            //Logger.getLogger(FrmAlbums.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizarAlbums(String selectedId, String nombre, String fecha, String idArtista, String idDisquera) {
        idArtista = Com_Art.getSelectedItem().toString().trim();
        idDisquera = Com_Dis.getSelectedItem().toString().trim();
        SeleccionarArtista(idArtista);
        SeleccionarDisquera(idDisquera);

        String stmtSQL = "update albums set nombre='" + nombre + "', fecha='" + fecha + "',Artistas_idArtista= '" + idArt + "',"
                + "Artistas_Disqueras_idDisquera= '" + idDis + "' where idAlbum = " + selectedId + ";";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en actualizacion de registro");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        this.selectedRow = -1;
        this.cargarAlbums();
    }

    private void eliminarAlbums(String selectedId) {
        String stmtSQL = "delete from albums where idAlbum = " + selectedId + ";";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en eliminacion de registro");
            } else {
                this.selectedRow = -1;
                this.cargarAlbums();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void cargarAlbums() {
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
            this.bd.res = bd.stat.executeQuery("select * from albums");
            while (bd.res.next()) {
                int idAlbum = bd.res.getInt("idAlbum");
                String nombre = bd.res.getString("nombre");
                String fecha = bd.res.getString("fecha");
                int idArtista = bd.res.getInt("Artistas_idArtista");
                int idDisquera = bd.res.getInt("Artistas_Disqueras_idDisquera");
                Object[] obj = {idAlbum, nombre, fecha, idArtista, idDisquera};
                modeloTabla.addRow(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void cargarCombo() {
        modeloCombo1 = (DefaultComboBoxModel) Com_Art.getModel();
        modeloCombo1.removeAllElements();
        try {
            this.bd.res = bd.stat.executeQuery("select nombre from Artistas where idArtista");
            while (bd.res.next()) {
                String nombre = bd.res.getString("nombre");
                modeloCombo1.addElement(nombre);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        modeloCombo2 = (DefaultComboBoxModel) Com_Dis.getModel();
        modeloCombo2.removeAllElements();
        try {
            this.bd.res = bd.stat.executeQuery("select nombre from Disqueras where idDisquera");
            while (bd.res.next()) {
                String nombre = bd.res.getString("nombre");
                modeloCombo2.addElement(nombre);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void filtrarAlbums(String _nombre) {
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
            this.bd.res = bd.stat.executeQuery("select idAlbum, nombre, fecha, Artistas_idArtista, "
                    + "Artistas_Disqueras_idDisquera from Albums where nombre like '%" + _nombre.trim() + "%'");
            while (bd.res.next()) {
                int idAlbum = bd.res.getInt("idAlbum");
                String nombre = bd.res.getString("nombre");
                String fecha = bd.res.getString("fecha");
                int idArtista = bd.res.getInt("Artistas_idArtista");
                int idDisquera = bd.res.getInt("Artistas_Disqueras_idDisquera");
                Object[] obj = {idAlbum, nombre, fecha, idArtista, idDisquera};
                modeloTabla.addRow(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void iniciarTablaValores() {
        this.jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                selectedRow = jTable1.getSelectedRow();

                if (selectedRow >= 0) {
                    selectedId = jTable1.getValueAt(selectedRow, 0).toString();
                    jTextField1.setText(jTable1.getValueAt(selectedRow, 1).toString());
                    jTextField2.setText(jTable1.getValueAt(selectedRow, 2).toString());
                    Com_Art.setSelectedItem(jTable1.getValueAt(selectedRow, 3).toString());
                    Com_Dis.setSelectedItem(jTable1.getValueAt(selectedRow, 4).toString());
                }
            }
        });
    }

    /**
     * Creates new form FrmDisqueras
     */
    public FrmAlbums() {
        this.bd = new ConexionBD();
        this.bd.conectar();
        initComponents();
        this.cargarAlbums();
        cargarCombo();
        setLocationRelativeTo(null);
        iniciarTablaValores();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        Com_Art = new javax.swing.JComboBox<>();
        Com_Dis = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Albums");

        jLabel1.setText("Nombre del Album");

        jToolBar1.setFloatable(false);

        jButton1.setText("Filtrar");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setText("Agregar");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setText("Guardar");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setText("Eliminar");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton5.setText("Actualizar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setText("Salir");
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Fecha", "IdArtista", "IdDisquera"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(10);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel2.setText("Artistas:");

        jLabel3.setText("Disquera:");

        jLabel4.setText("Fecha:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 822, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(Com_Art, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(Com_Dis, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addGap(2, 2, 2)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGap(2, 2, 2)
                        .addComponent(Com_Art, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(2, 2, 2)
                        .addComponent(Com_Dis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        this.cargarAlbums();
        cargarCombo();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.agregarAlbums(jTextField1.getText(), jTextField2.getText(), Com_Art.getSelectedItem().toString(), Com_Dis.getSelectedItem().toString());
        this.cargarAlbums();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow() >= 0) {
            this.actualizarAlbums(this.selectedId, jTextField1.getText(), jTextField2.getText(), Com_Art.getSelectedItem().toString(), Com_Dis.getSelectedItem().toString());
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow() >= 0) {
            this.eliminarAlbums(this.selectedId);
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.filtrarAlbums(this.jTextField1.getText());

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        new Principal().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAlbums.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAlbums.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAlbums.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAlbums.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmAlbums().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Com_Art;
    private javax.swing.JComboBox<String> Com_Dis;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

}
