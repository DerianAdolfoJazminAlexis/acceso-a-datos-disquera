package topicos.view;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import topicos.model.ConexionBD;

public class FrmCanciones extends javax.swing.JFrame {

    DefaultTableModel modeloTabla;
    DefaultComboBoxModel modeloCombo1;
    DefaultComboBoxModel modeloCombo2;
    DefaultComboBoxModel modeloCombo3;
    ConexionBD bd;
    int selectedRow = -1;
    int idArt = 0;
    int idDis = 0;
    int idAl = 0;
    String selectedId;

    public void SeleccionarDisquera(String nombre) {
        try {
            this.bd.res = bd.stat.executeQuery("select idDisquera from Disqueras where nombre = '" + nombre + "';");
            while (bd.res.next()) {
                idDis = bd.res.getInt("idDisquera");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void SeleccionarArtista(String nombre) {
        try {
            this.bd.res = bd.stat.executeQuery("select idArtista from Artistas where nombre = '" + nombre + "';");
            while (bd.res.next()) {
                idArt = bd.res.getInt("idArtista");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void SeleccionarAlbum(String nombre) {
        try {
            this.bd.res = bd.stat.executeQuery("select idAlbum from Albums where nombre = '" + nombre + "';");
            while (bd.res.next()) {
                idAl = bd.res.getInt("idAlbum");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void agregarCanciones(String nombre, String duracion, String idAlbum, String idArtista, String idDisquera) {
        idAlbum = Com_Alb.getSelectedItem().toString().trim();
        idArtista = Com_Art.getSelectedItem().toString().trim();
        idDisquera = Com_Dis.getSelectedItem().toString().trim();
        SeleccionarAlbum(idAlbum);
        SeleccionarArtista(idArtista);
        SeleccionarDisquera(idDisquera);

        String stmtSQL = "INSERT INTO `canciones` (`nombre`, `duracion`, `Albums_idAlbum`, `Albums_Artistas_idArtista`, `Albums_Artistas_Disqueras_idDisquera`) "
                + "VALUES ('"+nombre+"', '"+duracion+"', '"+idAl+"', '"+idArt+"', '"+idDis+"');";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en insercion de registro");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ActualizarCanciones(String selectedId, String nombre, String duracion, String idAlbum, String idArtista, String idDisquera) {
        idAlbum = Com_Alb.getSelectedItem().toString().trim();
        idArtista = Com_Art.getSelectedItem().toString().trim();
        idDisquera = Com_Dis.getSelectedItem().toString().trim();
        SeleccionarAlbum(idAlbum);
        SeleccionarArtista(idArtista);
        SeleccionarDisquera(idDisquera);

        String stmtSQL = "UPDATE `canciones` SET `nombre`='"+nombre+"',`duracion`="+duracion+","
                + "`Albums_idAlbum`="+idAl+",`Albums_Artistas_idArtista`="+idArt+",`Albums_Artistas_Disqueras_idDisquera`="+idDis+" "
                + "WHERE idCancion = "+selectedId+";";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en actualizacion de registro");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.selectedRow = -1;
        this.cargarCanciones();
    }

    private void eliminarCanciones(String selectedId) {
        String stmtSQL = "delete from canciones where idCancion = " + selectedId + ";";
        try {
            if (bd.stat.execute(stmtSQL)) {
                JOptionPane.showMessageDialog(this, "Error en eliminacion de registro");
            } else {
                this.selectedRow = -1;
                this.cargarCanciones();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargarCanciones() {
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
            this.bd.res = bd.stat.executeQuery("select * from canciones");
            while (bd.res.next()) {
                int idCancion = bd.res.getInt("idCancion");
                String nombre = bd.res.getString("nombre");
                String duracion = bd.res.getString("duracion");
                int idAlbum = bd.res.getInt("Albums_idAlbum");
                int idArtista = bd.res.getInt("Albums_Artistas_idArtista");
                int idDisquera = bd.res.getInt("Albums_Artistas_Disqueras_idDisquera");
                Object[] obj = {idCancion, nombre, duracion, idAlbum, idArtista, idDisquera};
                modeloTabla.addRow(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargarCombo() {
        modeloCombo1 = (DefaultComboBoxModel) Com_Art.getModel();
        modeloCombo1.removeAllElements();
        try {
            this.bd.res = bd.stat.executeQuery("select nombre from Artistas where idArtista");
            while (bd.res.next()) {
                String nombre = bd.res.getString("nombre");
                modeloCombo1.addElement(nombre);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        modeloCombo2 = (DefaultComboBoxModel) Com_Dis.getModel();
        modeloCombo2.removeAllElements();
        try {
            this.bd.res = bd.stat.executeQuery("select nombre from Disqueras where idDisquera");
            while (bd.res.next()) {
                String nombre = bd.res.getString("nombre");
                modeloCombo2.addElement(nombre);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }

        modeloCombo3 = (DefaultComboBoxModel) Com_Alb.getModel();
        modeloCombo3.removeAllElements();
        try {
            this.bd.res = bd.stat.executeQuery("select nombre from Albums where idAlbum");
            while (bd.res.next()) {
                String nombre = bd.res.getString("nombre");
                modeloCombo3.addElement(nombre);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void filtrarCanciones(String _nombre) {
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
            this.bd.res = bd.stat.executeQuery("select idAlbum, nombre, duracion,Albums_idAlbum,Albums_Artistas_idArtista,Albums_Artistas_Disqueras_idDisquera "
                    + "from Canciones where nombre like '%" + _nombre.trim() + "%'");
            while (bd.res.next()) {
                int idCancion = bd.res.getInt("idCancion");
                String nombre = bd.res.getString("nombre");
                String duracion = bd.res.getString("duracion");
                int idAlbum = bd.res.getInt("Albums_idAlbum");
                int idArtista = bd.res.getInt("Albums_Artistas_idArtista");
                int idDisquera = bd.res.getInt("Albums_Artistas_Disqueras_idDisquera");
                Object[] obj = {idCancion, nombre, duracion, idAlbum, idArtista, idDisquera};
                modeloTabla.addRow(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmCanciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void iniciarTablaValores() {
        this.jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                selectedRow = jTable1.getSelectedRow();
                if (selectedRow >= 0) {
                    selectedId = jTable1.getValueAt(selectedRow, 0).toString();
                    jTextField1.setText(jTable1.getValueAt(selectedRow, 1).toString());
                    jTextField2.setText(jTable1.getValueAt(selectedRow, 2).toString());
                    Com_Alb.setSelectedItem(jTable1.getValueAt(selectedRow, 3).toString());
                    Com_Art.setSelectedItem(jTable1.getValueAt(selectedRow, 4).toString());
                    Com_Dis.setSelectedItem(jTable1.getValueAt(selectedRow, 5).toString());
                }
            }
        });
    }

    /**
     * Creates new form FrmDisqueras
     */
    public FrmCanciones() {
        this.bd = new ConexionBD();
        this.bd.conectar();
        initComponents();
        this.cargarCanciones();
        cargarCombo();
        setLocationRelativeTo(null);
        iniciarTablaValores();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        Com_Art = new javax.swing.JComboBox<>();
        Com_Dis = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Com_Alb = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Canciones");

        jLabel1.setText("Nombre de la Cancion:");

        jToolBar1.setFloatable(false);

        jButton1.setText("Filtrar");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setText("Agregar");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setText("Guardar");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setText("Eliminar");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton5.setText("Actualizar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setText("Salir");
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Duracion", "IdAlbum", "IdArtista", "IdDisquera"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(10);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel2.setText("Artistas:");

        jLabel3.setText("Disquera:");

        jLabel4.setText("Duracion: ");

        jLabel5.setText("Album:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 822, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(Com_Art, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(Com_Dis, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(Com_Alb, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addGap(2, 2, 2)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addGap(2, 2, 2)
                        .addComponent(Com_Alb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGap(2, 2, 2)
                        .addComponent(Com_Art, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(2, 2, 2)
                        .addComponent(Com_Dis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        this.cargarCanciones();
        cargarCombo();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.agregarCanciones(jTextField1.getText(), jTextField2.getText(), Com_Alb.getSelectedItem().toString(), Com_Art.getSelectedItem().toString(), Com_Dis.getSelectedItem().toString());
        this.cargarCanciones();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow() >= 0) {
            this.ActualizarCanciones(this.selectedId, jTextField1.getText(), jTextField2.getText(), Com_Alb.getSelectedItem().toString(), Com_Art.getSelectedItem().toString(), Com_Dis.getSelectedItem().toString());
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow() >= 0) {
            this.eliminarCanciones(this.selectedId);
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.filtrarCanciones(this.jTextField1.getText());

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        new Principal().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmCanciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Com_Alb;
    private javax.swing.JComboBox<String> Com_Art;
    private javax.swing.JComboBox<String> Com_Dis;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

}
